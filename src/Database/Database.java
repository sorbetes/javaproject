package Database;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import OrderClient.NewOrderSingle;
import OrderManager.Order;

//TODO figure out how to make this abstract or an interface, but want the method to be static
public class Database{
	
	private static final String header="ClientID,OrderID,Side,Instrument,Price,Size,Status\n";
	private static FileOutputStream outputfile_orders;
	
	public static void generateNewOrderFile() throws FileNotFoundException, IOException {
		outputfile_orders=new FileOutputStream("orders.csv");
		byte[] databyte=header.getBytes();
		outputfile_orders.write(databyte);
	}
	
	public static void writeNewOrder(int id, int ordid, NewOrderSingle o) throws IOException{
		String line=id+","+ordid+","+o.side+","+o.instrument.ric.ric+","+o.price+","+o.size+",new\n";
		outputfile_orders.write(line.getBytes());
	}
	public static void writeMatchOrder(int size, Order o) throws IOException {	//any fill by router
		String line=o.clientid+","+o.clientOrderID+","+o.side+","+o.instrument.ric.ric+","+o.initialMarketPrice+","+size+","+o.ordStatus+"\n";
		outputfile_orders.write(line.getBytes());
	}
	public static void writeMatchOrder(Order o) throws IOException { //only when fully filled by cross/trader
		String line=o.clientid+","+o.clientOrderID+","+o.side+","+o.instrument.ric.ric+","+o.initialMarketPrice+","+o.sizeFilled()+","+o.ordStatus+"\n";
		outputfile_orders.write(line.getBytes());
	}
}