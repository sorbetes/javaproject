package OrderManager;

import java.io.Serializable;
import java.util.ArrayList;

import Ref.Instrument;

public class Order implements Serializable{
	public int id;
	public int clientOrderID;
	public int clientid;
	
	short orderRouter;
	int size;
	double[]bestPrices;
	int bestPriceCount;
	
	public Instrument instrument;
	public double initialMarketPrice;
	public ArrayList<Order>slices;
	ArrayList<Fill>fills;
	public char ordStatus='A'; //OrdStatus is Fix 39, 'A' = Pending new, '0' = New, '1' = Partial fill, '2' = Filled
	public final int side;
	public String ordType;
	
	/**
	 * CONSTRUCTOR=
	 * @param clientid The id of the client whose order this belongs to.
	 * @param clientOrderID ID of the order.
	 * @param instrument The instrument to be traded.
	 * @param size The amount of the instrument to be sold/bought.
	 */
	public Order(int clientid, int clientOrderID, Instrument instrument, int size, int side){
		this.clientOrderID=clientOrderID;
		this.size=size;
		this.clientid=clientid;
		this.instrument=instrument;
		fills=new ArrayList<Fill>();
		slices=new ArrayList<Order>();
		this.side = side;
		ordType = "IX";
	}
	
	/**
	 * Gets the status of the order.
	 * @return The order status code.
	 */
	public char getOrdStatus() { return ordStatus; }
	
	/**
	 * Gets the type of the order.
	 * @return "AT" if the order is out to market, "IX" if the order is internal.
	 */
	public String getOrdType() { return ordType; }
	
	/**
	 * Sets the type of the order to market status.
	 */
	public void setToMarket() { ordType = "AT"; }
	
	/**
	 * Sets the type of the order to internal status.
	 */
	public void setToInternal() {ordType = "IX"; }
	
	/**
	 * Gets the total size of all the current slices.
	 * @return Total size of all the slices.
	 */
	public int sliceSizes(){
		int totalSizeOfSlices=0;
		for(Order c:slices)totalSizeOfSlices+=c.size;
		return totalSizeOfSlices;
	}
	
	/**
	 * Creates a new order slice of a given size and stores it in the array. 
	 * @param sliceSize The size of the slice.
	 * @return The index position of the new slice in the array.
	 * @throws Exception 
	 */
	public int newSlice(int sliceSize) throws Exception{
		if (size - sliceSizes() < sliceSize) {
			throw new Exception("Remaining order size less than slice size given.");
		}
		slices.add(new Order(id,clientOrderID,instrument,sliceSize,side));
		return slices.size()-1;
	}
	
	/**
	 * Gets how much of the order has been filled.
	 * @return The amount of the order that has been filled.
	 */
	public int sizeFilled(){
		int filledSoFar=0;
		for(Fill f:fills){
			filledSoFar+=f.size;
		}
		for(Order c:slices){
			filledSoFar+=c.sizeFilled();
		}
		return filledSoFar;
	}
	
	/**
	 * Gets the remaining amount of the order that hasn't been filled yet.
	 * @return The remaining amount of the order thats not been filled.
	 */
	public int sizeRemaining(){
		return size-sizeFilled();
	}
	
	/**
	 * Gets the amount of the order that is currently at the market.
	 * @return The size that is at the market
	 */
	public int sizeAtMarket() {
		int total = 0;
		for(Order c:slices){
			if (c.getOrdType().equals("AT")) {
				total += c.sizeRemaining();
			}
		}
		
		return total;
	}
	
	/**
	 * Gets the average price of all the fills.
	 * @return Average price of the fills.
	 */
	float price(){
		float sum=0;
		int amount = 0;
		for(Fill fill:fills){
			sum+=fill.price;
			++amount;
		}
		for (Order c : slices) {
			for (Fill fill:c.fills) {
				sum += fill.price;
				++amount;
			}
		}
		return sum/amount;
	}
	
	/**
	 * Creates a new fill order of a given price and size.
	 * @param size The amount if the instrument for the fill.
	 * @param price The price of a single instrument.
	 */
	void createFill(int size,double price){
		fills.add(new Fill(size,price));
		if(sizeRemaining()==0){
			ordStatus='2';
		}else{
			ordStatus='1';
		}
	}
	
	/**
	 * Used to perform an internal cross between this order and another that have a matching price.
	 * @param matchingOrder The order to be crossed with.
	 */
	void cross(Order matchingOrder){
		// Used as the starting index for the matching orders slice index.
		// Stops looping through matching order slices that are already complete.
		int currMatchSlice = 0;
		
		for(Order slice:slices){
			if(slice.sizeRemaining()==0)continue;
			for(; currMatchSlice < matchingOrder.slices.size(); ++currMatchSlice){
				Order matchingSlice = matchingOrder.slices.get(currMatchSlice);
				int msze = matchingSlice.sizeRemaining();
				if(msze==0)continue;
				int sze=slice.sizeRemaining();
				if(sze<=msze){
					 slice.createFill(sze,initialMarketPrice);
					 matchingSlice.createFill(sze, initialMarketPrice);
					 break;
				} else {
					slice.createFill(msze,initialMarketPrice);
					matchingSlice.createFill(msze, initialMarketPrice);
				}
			}
			int sze=slice.sizeRemaining();
			int mParent=matchingOrder.sizeRemaining();
			if(sze>0 && mParent>0){
				if(sze<=mParent){
					slice.createFill(sze,initialMarketPrice);
					matchingOrder.createFill(sze, initialMarketPrice);
				}else{
					slice.createFill(mParent,initialMarketPrice);
					matchingOrder.createFill(mParent, initialMarketPrice);					
				}
			}
			// Matching order is full, no point continuing.
			if(slice.sizeRemaining()>0)break;
		}
		if(sizeRemaining()>0){
			for(; currMatchSlice < matchingOrder.slices.size(); ++currMatchSlice){
				Order matchingSlice = matchingOrder.slices.get(currMatchSlice);
				int msze=matchingSlice.sizeRemaining();
				if(msze==0)continue;
				int sze=sizeRemaining();
				if(sze<=msze){
					 createFill(sze,initialMarketPrice);
					 matchingSlice.createFill(sze, initialMarketPrice);
					 break;
				} else {
					createFill(msze,initialMarketPrice);
					matchingSlice.createFill(msze, initialMarketPrice);
				}
			}
			int sze=sizeRemaining();
			int mParent=matchingOrder.sizeRemaining();
			if(sze>0 && mParent>0){
				if(sze<=mParent){
					createFill(sze,initialMarketPrice);
					matchingOrder.createFill(sze, initialMarketPrice);
				}else{
					createFill(mParent,initialMarketPrice);
					matchingOrder.createFill(mParent, initialMarketPrice);					
				}
			}
		}
	}
	
	void cancel(){
		//state=cancelled
	}
}

class Basket{
	Order[] orders;
}

class Fill implements Serializable{
	//long id;
	int size;
	double price;
	Fill(int size,double price){
		this.size=size;
		this.price=price;
	}
}