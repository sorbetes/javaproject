package OrderManager;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.IntSummaryStatistics;
import java.util.Map;

import Database.Database;
import Enum.traderApi;
import LiveMarketData.LiveMarketData;
import LiveMarketData.LiveMarketDataMessage;
import Logging.M3Log;
import OrderClient.NewOrderSingle;
import OrderRouter.Router;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrderManager {
	
	private HashMap<Integer,Order> orders=new HashMap<Integer,Order>(); //debugger will do this line as it gives state to the object
	//currently recording the number of new order messages we get. TODO why? use it for more?
	private int id=0; //debugger will do this line as it gives state to the object
	private Socket[] orderRouters; //debugger will skip these lines as they dissapear at compile time into 'the object'/stack
	private Socket[] clients;
	private Socket trader;
	private Socket marketData;

	private Socket connect(InetSocketAddress location) throws InterruptedException {
		boolean connected = false;
		int tryCounter = 0;
		while (!connected && tryCounter < 600) {
			try {
				Socket s = new Socket(location.getHostName(), location.getPort());
				s.setKeepAlive(true);
				return s;
			} catch (IOException e) {
				Thread.sleep(1000);
				tryCounter++;
			}
		}
		System.out.println("Failed to connect to " + location.toString());
		M3Log.lg.info("Failed to connect...");
		return null;
	}

	//@param args the command line arguments
	/**
	 * Constructor of OrderManager
	 * Connects to all the trader socket, market data socket, router sockets, and client sockets
	 * Loops through the clients, routers, trader, and market data and reads from their outputstream until program exits
	 * Handles their message accordingly
	 * @param orderRouters array of router sockets
	 * @param clients array of client sockets
	 * @param trader trader socket
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 */
	public OrderManager(InetSocketAddress[] orderRouters, InetSocketAddress[] clients,InetSocketAddress trader)throws IOException, ClassNotFoundException, InterruptedException{
		
		//create a file to store orders
		Database.generateNewOrderFile();
		
		M3Log.lg.info("Booting up.");
		this.trader = connect(trader);
		if (!this.trader.equals(null))
			M3Log.lg.info("Connected to the trader!");
		this.marketData = connect(new InetSocketAddress(6969));
		if (!this.marketData.equals(null))
			M3Log.lg.info("Connected to the Market Data!");
		// for the router connections, copy the input array into our object field.
		// but rather than taking the address we create a socket+ephemeral port and
		// connect it to the address
		this.orderRouters = new Socket[orderRouters.length];
		int i = 0; // need a counter for the the output array
		for (InetSocketAddress location : orderRouters) {
			this.orderRouters[i] = connect(location);
			i++;
		}

		// repeat for the client connections
		this.clients = new Socket[clients.length];
		i = 0;
		for (InetSocketAddress location : clients) {
			this.clients[i] = connect(location);
			i++;
		}
		int clientId,routerId;
		Socket client,router;
		//main loop, wait for a message, then process it
		while(true){
			//TODO this is pretty cpu intensive, use a more modern polling/interrupt/select approach
			for(clientId=0;clientId<this.clients.length;clientId++){ //check if we have data on any of the sockets
				client=this.clients[clientId];
				while (client.getInputStream().available() > 0){ //if we have part of a message ready to read, assuming this doesn't fragment messages
					ObjectInputStream is=new ObjectInputStream(client.getInputStream()); //create an object inputstream, this is a pretty stupid way of doing it, why not create it once rather than every time around the loop 
					String method=(String)is.readObject();
					System.out.println(Thread.currentThread().getName()+" calling "+method);
					switch(method){ //determine the type of message and process it
						//call the newOrder message with the clientId and the message (clientMessageId,NewOrderSingle)
						case "newOrderSingle": newOrder(clientId, is.readInt(), (NewOrderSingle)is.readObject());break;
						case "cancel": sendCancelOrder(clientId, is.readInt()); break;//client id and id of order to cancel
						default: System.out.println("Unknown message type.. "); break; //remove?resend?skip? //MsgType=3 if rejected TODO
					}
				}
			}
			for (routerId = 0; routerId < this.orderRouters.length; routerId++) { // check if we have data on any of the
																					// sockets
				router = this.orderRouters[routerId];
				if (0 < router.getInputStream().available()) { // if we have part of a message ready to read, assuming
																// this doesn't fragment messages
					ObjectInputStream is = new ObjectInputStream(router.getInputStream()); // create an object
																							// inputstream, this is a
																							// pretty stupid way of
																							// doing it, why not create
																							// it once rather than every
																							// time around the loop
					String method = (String) is.readObject();
					System.out.println(Thread.currentThread().getName() + " calling " + method);
					switch (method) { // determine the type of message and process it
					case "bestPrice":
						int OrderId = is.readInt();
						int SliceId = is.readInt();
						Order slice = orders.get(OrderId).slices.get(SliceId);
						slice.bestPrices[routerId] = is.readDouble();
						slice.bestPriceCount += 1;
						if (slice.bestPriceCount == slice.bestPrices.length)
							reallyRouteOrder(SliceId, slice);
						break;
					case "newFill":
						newFill(is.readInt(), is.readInt(), is.readInt(), is.readDouble());
						break;
					// case cancelled
					}
				}
			}

			if (0 < this.trader.getInputStream().available()) {
				System.out.println("Getting a message from the trader");
				ObjectInputStream is = new ObjectInputStream(this.trader.getInputStream());
				String method = (String) is.readObject();
				System.out.println(Thread.currentThread().getName() + " calling " + method);
				switch (method) {
				case "acceptOrder":
					acceptOrder(is.readInt());
					break;
				case "sliceOrder":
					sliceOrder(is.readInt(), is.readInt());
					break;
				case "crossOrder":
					internalCross(is.readInt(), is.readInt());
					break;
				case "routeOrder":
					routeOrder(is.readInt(), (Order) is.readObject());
					break;
				case "routeOrderSlice":
					routeOrder(is.readInt(), is.readInt(), (Order) is.readObject());
					break;
				}
			}
		}
	}


	/**
	 * Adds the new order to the list of orders received by the OrderManager
	 * Sends a message to the client that the order has been received
	 * Sends the order to the trader
	 * @param clientId Client ID, index of the client in the OrderManager's list
	 * @param clientOrderId Index of client in client list stored in SampleClient class
	 * @param nos Order details
	 * @throws IOException
	 */
	private void newOrder(int clientId, int clientOrderId, NewOrderSingle nos) throws IOException{
		Order temp= new Order(clientId, clientOrderId, nos.instrument, nos.size, nos.side);
		temp.initialMarketPrice=nos.price;
		orders.put(id, temp);
		Database.writeNewOrder(clientId, clientOrderId, nos);
		//send a message to the client with 39=A; //OrdStatus is Fix 39, 'A' is 'Pending New'
		ObjectOutputStream os=new ObjectOutputStream(clients[clientId].getOutputStream());
		//newOrderSingle acknowledgement
		//ClOrdId is 11=
		os.writeObject("11="+clientOrderId+";35=A;39=A;");
		os.flush();
		sendOrderToTrader(id, orders.get(id), traderApi.newOrder);
		// send the new order to the trading screen
		// don't do anything else with the order, as we are simulating high touch orders
		// and so need to wait for the trader to accept the order
		id++;
	}
	/**
	 * Creates output stream to send the order to the trader
	 * @param id Index of order in OrderManager's list of orders
	 * @param o The order to be sent
	 * @param method The message to trader?? os that trader knows what to do with it??
	 * @throws IOException
	 */
	private void sendOrderToTrader(int id, Order o, Object method) throws IOException {
		ObjectOutputStream ost = new ObjectOutputStream(trader.getOutputStream());
		ost.writeObject(method);
		ost.writeInt(id);
		ost.writeObject(o);
		ost.flush();
	}
	/**
	 * Sends message to the trader to cancel the order
	 * @param id Order ID of the client
	 * @param method
	 * @throws IOException
	 */
	private void sendCancelToTrader(int id, Object method) throws IOException {
		ObjectOutputStream ost = new ObjectOutputStream(trader.getOutputStream());
		ost.writeObject(method);
		ost.writeInt(id);
		ost.flush();
	}
	

	/**
	 * Creates output stream to send the order to the trader
	 * @param id Index of order in OrderManager's list of orders
	 * @param o The order to be sent
	 * @param method The message to trader?? os that trader knows what to do with it??
	 * @throws IOException
	 */
	private void sendOrderToTraderFill(int id, Order o) throws IOException {
		ObjectOutputStream ost = new ObjectOutputStream(trader.getOutputStream());
		ost.writeObject(traderApi.fill);
		ost.writeInt(id);
		ost.writeObject(o);
		ost.writeObject(o.slices.get(o.slices.size() - 1));
		ost.flush();
	}


	/**
	 * Order has been accepted by the trader. Send message to client about the status of the order (accepted).
	 * @param id Client ID, index of the client in the OrderManager's list
	 * @throws IOException
	 */
	public void acceptOrder(int id) throws IOException{
		System.out.println("acceptOrder");
		Order o=orders.get(id);
		if(o.ordStatus!='A'){ //Pending New
			M3Log.lg.info("Error: Accepting order that has already been accepted");
			return;
		}
		o.ordStatus='0'; //New
		ObjectOutputStream os=new ObjectOutputStream(clients[o.clientid].getOutputStream());
		//newOrderSingle acknowledgement
		//ClOrdId is 11=
		os.writeObject("11="+o.clientOrderID+";35=A;39=0");
		os.flush();
	}
	/**
	 * Router sent a message that there is a new fill.
	 * Write to csv file and send message to client about the status of the order
	 * @param id Client ID, index of the client in the OrderManager's list
	 * @param sliceId
	 * @param size
	 * @param price
	 * @throws IOException
	 */
	private void newFill(int id,int sliceId,int size,double price) throws IOException{
		//from router
		System.out.println("newfill");
		Order o=orders.get(id);
		o.slices.get(sliceId).createFill(size, price);
		//o.newSlice(sliceSize); TODO
		if(o.sizeRemaining()==0){
			Database.writeMatchOrder(size, o); //TODO
			//Send message to client
			o.ordStatus='2';
			ObjectOutputStream os=new ObjectOutputStream(clients[id].getOutputStream());
			os.writeObject("11="+o.clientOrderID+";35=B;39=2");
			os.flush();
		} else {
			//Send message to client
			Database.writeMatchOrder(size, o); //TODO
			o.ordStatus='1';
			ObjectOutputStream os=new ObjectOutputStream(clients[id].getOutputStream());
			os.writeObject("11="+o.clientOrderID+";35=B;39=1");
			os.flush();
		}
		orders.remove(id);
		orders.put(id, o);
		
		sendOrderToTraderFill(id, orders.get(id));
	}
	/**
	 * Received a cancel order request from client.
	 * Will let trader and router know about this cancel.
	 * @param clientId Client ID, index of the client in the OrderManager's list
	 * @param id Order Id of the order to be cancelled
	 * @throws IOException
	 */
	private void sendCancelOrder(int clientId, int id) throws IOException{
		//need to check if order exists
		//orders.remove(id); //remove from orders list
		orders.get(id).cancel(); //call cancel() from order class
		ObjectOutputStream os=new ObjectOutputStream(clients[clientId].getOutputStream());
		os.writeObject("11="+id+";35=F;39=6;"); //F:order cancel request, 6:pending cancel
		os.flush();
		
		sendCancelToTrader(id, traderApi.cancel); //tell trader to cancel

		//orderRouter.sendCancel(order);
		//order.orderRouter.writeObject(order);
		
		//if receive go signal from trader and router, it's been cancelled, cancelledOrder(Order o);
	}
	/**
	 * Sends a message to the client that the order has been cancelled
	 * @param order
	 * @throws IOException
	 */
	private void cancelledOrder(int clientid, Order order) throws IOException{
		System.out.println("cancelled, success");
		//Send message to client
		order.ordStatus='4';
		ObjectOutputStream os=new ObjectOutputStream(clients[clientid].getOutputStream());
		os.writeObject("11="+order.clientOrderID+";35=B;39=4");
		os.flush();
		//send message how much filled
		//os.writeInt(orders.get(id).sizeFilled());
	}
	/**
	 * Sends a message to the client that the cancel request has been rejected.
	 * @param clientid
	 * @param order
	 * @throws IOException
	 */
	private void cancelRequestRejected(int clientid, Order order) throws IOException {
		order.ordStatus='8';
		ObjectOutputStream os=new ObjectOutputStream(clients[clientid].getOutputStream());
		os.writeObject("11="+order.clientOrderID+";35=9;39=8");
		os.flush();
	}
	public void sliceOrder(int id,int sliceSize) throws IOException{
		System.out.println("slice");
		try { 
			Order o=orders.get(id);
			o.newSlice(sliceSize); 
			sendOrderToTrader(id, o, traderApi.slice);
		} catch (Exception e) {
			M3Log.lg.info("Error: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void sliceOrder(int id, int sliceSize, Order order) throws IOException{
		System.out.println("slice");
		try { 
			order.newSlice(sliceSize); 
			orders.remove(id);
			orders.put(id, order);
			sendOrderToTrader(id, order, traderApi.slice);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Matching the order with the orders in orderlist to check if there are any matching trades.
	 * @param id 
	 * @param o
	 * @throws IOException
	 */
	private void internalCross(int id, Order o) throws IOException{
		for(Map.Entry<Integer, Order>entry:orders.entrySet()){
			if(entry.getKey().intValue()==id)continue;
			Order matchingOrder=entry.getValue();
			if(!(matchingOrder.instrument.equals(o.instrument)&&matchingOrder.initialMarketPrice==o.initialMarketPrice))continue;
			if(matchingOrder.side==o.side)continue;
			//TODO add support here and in Order for limit orders
			int sizeBefore=o.sizeRemaining();
			o.cross(matchingOrder);
			if (sizeBefore != o.sizeRemaining()) {
				sendOrderToTrader(id, o, traderApi.cross);
			}
		}
	}
	private void internalCross(int buyId, int sellId) throws IOException {
		System.out.println("internalcross");
		Order buy = orders.get(buyId); 
		Order sell = orders.get(sellId);
		buy.cross(sell);
		if (buy.sizeRemaining()==0) { //or sell?
			Database.writeMatchOrder(buy);			
		}
		sendOrderToTrader(buyId, buy, traderApi.cross);
		sendOrderToTrader(sellId, sell, traderApi.cross);
	}
	/**
	 * Will send an order to an order router. The order must be a slice and not a
	 * parent order.
	 * 
	 * @param id      ???
	 * @param sliceId ???
	 * @param order   The order slice to be sent to the order router.
	 * @throws IOException
	 */
	private void routeOrder(int id, int sliceId, Order order) throws IOException {
		System.out.println("routeorder");
		//only connects to the LSE
		ObjectOutputStream os=new ObjectOutputStream(orderRouters[0].getOutputStream());
		os.writeObject(Router.routerApi.routeOrder);
		os.writeInt(id);
		os.writeInt(sliceId);
		os.writeInt(order.size);
		os.writeObject(order.instrument);
		os.flush();

		// need to wait for these prices to come back before routing
		order.bestPrices = new double[orderRouters.length];
		order.bestPriceCount = 0;
	}

	private void routeOrder(int id, Order order) throws IOException {
		System.out.println("routeorder2");
		// Root order - must make a slice of the total size of the order
		try { 
			order.newSlice(order.sizeRemaining()); 
			orders.remove(id);
			orders.put(id, order);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		int sliceId = order.slices.size() - 1;
		routeOrder(id, sliceId, order);
	}

	private void reallyRouteOrder(int sliceId, Order o){
		// TODO this assumes we are buying rather than selling
		int minIndex = 0;
		double min = o.bestPrices[0];
		for (int i = 1; i < o.bestPrices.length; i++) {
			if (min > o.bestPrices[i]) {
				minIndex = i;
				min = o.bestPrices[i];
			}
		}
		try {
			ObjectOutputStream os = new ObjectOutputStream(orderRouters[minIndex].getOutputStream());
			os.writeObject(Router.routerApi.routeOrder);
			os.writeInt(o.id);
			os.writeInt(sliceId);
			os.writeInt(o.sizeRemaining());
			os.writeObject(o.instrument);
			os.flush();
		}catch(IOException e) {
			M3Log.lg.info("Error: "+e.getMessage());
		}
	}

	/**
	 * <b>price</b>
	 * <p>
	 * The only method that, at time of writing (23/04/19, 14:14pm) communicates
	 * with the LiveMarketData thread. This method sets the price in the supplied
	 * order to whatever the LiveMarketData thread deems appropriate (also as of
	 * writing the price is always set to 4).
	 * </p>
	 * 
	 * @param id
	 * @param o
	 * @throws IOException
	 * @author Matt
	 */
	private void price(int id, Order o) throws IOException {
		// liveMarketData.setPrice(o);
		M3Log.lg.info("Beginning preparations to communicate with LiveMarketData.");
		ObjectOutputStream os = new ObjectOutputStream(marketData.getOutputStream());
		ObjectInputStream is = new ObjectInputStream(marketData.getInputStream());
		os.writeObject(new LiveMarketDataMessage("setPrice", o));
		os.flush();
		M3Log.lg.info("Data sent to LiveMarketData.");
		try {
			LiveMarketDataMessage message = (LiveMarketDataMessage) is.readObject();
			sendOrderToTrader(id, (Order) message.item, traderApi.price);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}