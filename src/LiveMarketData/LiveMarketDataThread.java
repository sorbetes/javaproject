package LiveMarketData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Random;
import java.util.Scanner;

import Logging.M3Log;
import OrderManager.Order;

/**
 * <b>LiveMarketDataThread</b>
 * <p>
 * This class' purpose is to allow LiveMarketData to run as a thread and take
 * information from over a web connection.
 * </p>
 * 
 */
public class LiveMarketDataThread extends Thread implements LiveMarketData {
	ServerSocketChannel ss;
	ObjectOutputStream os;
	ObjectInputStream is;
	public Random rng;

	public LiveMarketDataThread() throws IOException {
		Properties prop = new Properties();
		InputStream input = new FileInputStream("config.properties");
		prop.load(input);
		ss = ServerSocketChannel.open();
		InetSocketAddress ourAddress = new InetSocketAddress(Integer.parseInt(prop.getProperty("liveMarketDataSocketAddress")));
		ss.bind(ourAddress);
		M3Log.lg.info("MarketData booted up.");
		rng = new Random();
	}

	public void run() {
		try {
			M3Log.lg.info("Market Data is running.");
			SocketChannel socketChannel = ss.accept();
			Socket socket = socketChannel.socket();
			os = new ObjectOutputStream(socket.getOutputStream());
			is = new ObjectInputStream(socket.getInputStream());
			while (true) {
				LiveMarketDataMessage message = (LiveMarketDataMessage) is.readObject();
				readMessage(message);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void readMessage(LiveMarketDataMessage message) throws IOException, ClassNotFoundException {
		if (message.request.equals("setPrice")) {
			setPrice((Order) message.item);
		} else {
			throw new IOException("Invalid request type.");
		}
	}

	public double checkPrice(String ric) {
		try {
			Scanner scanner = new Scanner(new File("instrumentsdata.txt"));
			ArrayList<String[]> prices = new ArrayList<>();
			while (scanner.hasNextLine()) {
				prices.add(scanner.nextLine().split(" "));
			}
			scanner.close();
			for (String[] result : prices) {
				if (result[0].equals(ric)) {
					return Double.parseDouble(result[1]);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public void setPrice(Order o) {
		double price = checkPrice(o.instrument.toString());
		// Do we make the price higher or lower?
		// Coin flip. On tails, reduce the price by up to 3%. On heads, raise the price
		// by up to 3%.
		switch (rng.nextInt(2)) {
		case 0:
			o.initialMarketPrice = price - (rng.nextDouble() * (price * 0.03));
			break;
		case 1:
			o.initialMarketPrice = price + (rng.nextDouble() * (price * 0.03));
			break;
		}
		try {
			os.writeObject(new LiveMarketDataMessage("Reply", o));
			os.flush();
		} catch (IOException e) {
			System.out.println("IOException thrown when trying to send a reply in setPrice.");
		}
	}
}
