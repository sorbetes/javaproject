package LiveMarketData;

import java.io.Serializable;
/**
 * <b>LiveMarketDataMessage</b>
 * <p>This class' purpose is to allow communication between the LiveMarketData thread and the Order Manager. It takes in two variables on its creation;</p>
 * <nl><li>request (String), which states the method you wish to have the LiveMarketData do. It is set to "Reply" on the replies.</li>
 * <li>item (Object), which is whatever you wish for the LiveMarketData to act upon.</li></nl>
 * @author Matt
 *
 */
@SuppressWarnings("serial") public class LiveMarketDataMessage implements Serializable {
	public String request;
	public Object item;
	
	public LiveMarketDataMessage(String request, Object item){
		this.request=request;
		this.item=item;
	}
}
