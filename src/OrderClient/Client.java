package OrderClient;

import java.io.IOException;
import java.time.LocalDateTime;

public interface Client{
	//Outgoing messages
	int sendOrder()throws IOException;
	void sendCancel(int id);
	
	//Incoming messages
	void partialFill(int orderid);
	void fullyFilled(int orderid);
	void pendingCancelRequest(int orderid);
	void cancelled(int orderid);
	
	void messageHandler();
}