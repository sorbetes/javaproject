package OrderClient;

import java.io.Serializable;
import java.time.LocalDateTime;

import Ref.Instrument;

@SuppressWarnings("serial")
public class NewOrderSingle implements Serializable{
	public int size;
	public double price;
	public Instrument instrument;
	public int side;
	public LocalDateTime time;
	public NewOrderSingle(int size,double price,Instrument instrument, int side){
		this.size=size;
		this.price=price;
		this.instrument=instrument;
		this.side=side;
	}
}