package OrderManager;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import Ref.Instrument;
import Ref.Ric;

public class TestOrder {
	@Test public void testOrderSliceCreation() {
		Order sut = new Order(0, 0, new Instrument(new Ric("VOD.L")), 1000, 0);
		assertTrue("Slices not initialized correctly", sut.sliceSizes() == 0);
		
		try {
			sut.newSlice(200);
			assertTrue("Slice not correct size: " + sut.sliceSizes(), sut.sliceSizes() == 200);
			sut.newSlice(200);
			assertTrue("Slice not correct size: " + sut.sliceSizes(), sut.sliceSizes() == 400);
			sut.newSlice(200);
			assertTrue("Slice not correct size: " + sut.sliceSizes(), sut.sliceSizes() == 600);
			sut.newSlice(200);
			assertTrue("Slice not correct size: " + sut.sliceSizes(), sut.sliceSizes() == 800);
			sut.newSlice(200);
			assertTrue("Slice not correct size: " + sut.sliceSizes(), sut.sliceSizes() == 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			sut.newSlice(200);
			assertTrue("Slice sizes exceed order size: " + sut.sliceSizes(), sut.sliceSizes() == 1000);
		} catch (Exception e) {
			assertTrue("Got in exception for still added the slice: " + sut.sliceSizes(), sut.sliceSizes() == 1000);
		}
	}
	
	@Test public void testOrderCrossSutRemaining() {
		Instrument testInst = new Instrument(new Ric("VOD.L"));
		
		Order sut = new Order(0, 0, testInst, 1000, 0);
		Order otherOrder = new Order(0, 0, testInst, 500, 1);
		sut.cross(otherOrder);
		
		assertTrue("Sut doesn't have correct amount remaining: " + sut.sizeRemaining(), sut.sizeRemaining() == 500);
		assertTrue("Other order isn't filled: " + otherOrder.sizeRemaining(), otherOrder.sizeRemaining() == 0);
	}
	
	@Test public void testOrderCrossOtherRemaining() {
		Instrument testInst = new Instrument(new Ric("VOD.L"));
		
		Order sut = new Order(0, 0, testInst, 750, 0);
		Order otherOrder = new Order(0, 0, testInst, 1000, 1);
		sut.cross(otherOrder);
		
		assertTrue("Other doesn't have correct amount remaining: " + otherOrder.sizeRemaining(), otherOrder.sizeRemaining() == 250);
		assertTrue("Sut order isn't filled: " + sut.sizeRemaining(), sut.sizeRemaining() == 0);
	}
	
	@Test public void testOrderCrossSutRemainingSliced() {
		Instrument testInst = new Instrument(new Ric("VOD.L"));
		
		Order sut = new Order(0, 0, testInst, 1000, 0);
		try {
			sut.newSlice(200);
			sut.newSlice(350);
			sut.newSlice(400);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Order otherOrder = new Order(0, 0, testInst, 500, 1);
		try {
			otherOrder.newSlice(50);
			otherOrder.newSlice(150);
			otherOrder.newSlice(175);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sut.cross(otherOrder);
		
		assertTrue("Sut doesn't have correct amount remaining: " + sut.sizeRemaining(), sut.sizeRemaining() == 500);
		assertTrue("Other order isn't filled: " + otherOrder.sizeRemaining(), otherOrder.sizeRemaining() == 0);
	}
	
	@Test public void testOrderCrossOtherRemainingSliced() {
		Instrument testInst = new Instrument(new Ric("VOD.L"));
		
		Order sut = new Order(0, 0, testInst, 750, 0);
		try {
			sut.newSlice(200);
			sut.newSlice(150);
			sut.newSlice(275);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Order otherOrder = new Order(0, 0, testInst, 1000, 1);
		try {
			otherOrder.newSlice(200);
			otherOrder.newSlice(350);
			otherOrder.newSlice(400);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sut.cross(otherOrder);
		
		assertTrue("Other doesn't have correct amount remaining: " + otherOrder.sizeRemaining(), otherOrder.sizeRemaining() == 250);
		assertTrue("Sut order isn't filled: " + sut.sizeRemaining(), sut.sizeRemaining() == 0);
	}
	
	@Test public void testOrderSizeAtMarket() {
		Order sut = new Order(0, 0,  new Instrument(new Ric("VOD.L")), 1000, 0);

		int sendToMarket = 0;
		try {
			sut.newSlice(100);
			assertTrue("Slice not correct size: " + sut.sliceSizes(), sut.sliceSizes() == 100);
			sendToMarket = sut.newSlice(300);
			assertTrue("Slice not correct size: " + sut.sliceSizes(), sut.sliceSizes() == 400);
			sut.newSlice(200);
			assertTrue("Slice not correct size: " + sut.sliceSizes(), sut.sliceSizes() == 600);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		sut.slices.get(sendToMarket).setToMarket();
		assertTrue("Size at market incorrect: " + sut.sizeAtMarket(), sut.sizeAtMarket() == 300);
	}
}
