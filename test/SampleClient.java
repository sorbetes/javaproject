import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import Logging.M3Log;
import OrderClient.Client;
import OrderClient.NewOrderSingle;
import OrderManager.Order;
import Ref.Instrument;
import Ref.Ric;

public class SampleClient extends Mock implements Client{
	private static final Random RANDOM_NUM_GENERATOR=new Random();
	
	public static ArrayList<Instrument> INSTRUMENTS=new ArrayList<>();
	public static ArrayList<Double> PRICES=new ArrayList<>();
	
	private static final HashMap<Integer,NewOrderSingle> OUT_QUEUE=new HashMap<>(); //queue for outgoing orders, use this for??
	private int id=0; //message id number
	private Socket omConn; //connection to order manager
	
	/**
	 * Reads a text file with details of the instruments and uses these prices as reference when creating a buy or sell order.
	 * @throws IOException
	 */
	public static void generateInstrumentsdata() {
		try {
			BufferedReader br = new BufferedReader(new FileReader("instrumentsdata.txt"));
			String line=br.readLine();
			int i=0;
			while(line!=null){
				String[] data=line.split("\\s+");
				INSTRUMENTS.add(new Instrument(new Ric(data[0])));
				PRICES.add(Double.parseDouble(data[1]));
				++i;
				line=br.readLine();
			}
			System.out.println(INSTRUMENTS.size());
		}catch(FileNotFoundException e) {
			M3Log.lg.info("File not found.");
			//exit?
		}catch(IOException e) {
			M3Log.lg.info("Error."+e.getMessage());
		}
	}
	/**
	 * Connects the client to the OrderManager.
	 * @param port
	 * @throws IOException
	 */
	public SampleClient(int port){
		//OM will connect to us
		try {
			omConn=new ServerSocket(port).accept();
			System.out.println("OM connected to client port "+port);
		}catch(IOException e) {
			M3Log.lg.info("Error."+e.getMessage());
		}
	}
	/**
	 * Generates a new order and sends the order to the OrderManager.
	 * @throws IOException
	 */
	@Override
	public int sendOrder() {
		int size=((RANDOM_NUM_GENERATOR.nextInt(5000) + 99) / 100 ) * 100;
		int instid=RANDOM_NUM_GENERATOR.nextInt(INSTRUMENTS.size());
		Instrument instrument=INSTRUMENTS.get(instid);
		
		double pricerandpercentage=RANDOM_NUM_GENERATOR.nextDouble()*5;
		double price=PRICES.get(instid);
		price=(RANDOM_NUM_GENERATOR.nextInt(10)>3)?(price*(1+pricerandpercentage/100)):(price*(1-pricerandpercentage/100));
		NewOrderSingle nos=new NewOrderSingle(size,price,instrument,RANDOM_NUM_GENERATOR.nextInt(2));
		//nos.time=LocalDateTime.now();
		show("sendOrder: id="+id+" size="+size+" instrument="+INSTRUMENTS.get(instid).toString());
		OUT_QUEUE.put(id,nos);
		if(omConn.isConnected()){
			try {
				ObjectOutputStream os=new ObjectOutputStream(omConn.getOutputStream());
				os.writeObject("newOrderSingle");
				//os.writeObject("35=D;");
				os.writeInt(id);
				os.writeObject(nos);
				os.flush();				
			}catch(IOException e) {
				M3Log.lg.info("Error."+e.getMessage());
			}
		}
		return id++;
	}
	/**
	 * Sends a cancel message to the OrderManager to cancel order with the id of idToCancel
	 * @param idToCancel
	 */
	@Override
	public void sendCancel(int idToCancel){
		show("sendCancel: id="+idToCancel);
		if(omConn.isConnected()){
			try {
				ObjectOutputStream os=new ObjectOutputStream(omConn.getOutputStream());
				os.writeObject("cancel");
				os.writeInt(idToCancel);
				os.flush();
				//OMconnection.sendMessage("cancel",idToCancel);
			}catch(IOException e) {
				M3Log.lg.info("Error."+e.getMessage());
			}
		}
	}


	enum methods{newOrderSingleAcknowledgement,partialFill,fullyFilled,pendingCancelRequest,cancelled,dontKnow};
	/**
	 * Reads from the inputstream of OrderManager
	 * Parses the string to determine the OrderId, MsgType, and OrdStatus
	 * The method which it will call depends on the message received.
	 * 
	 */
	@Override
	public void messageHandler(){
		ObjectInputStream is;
		try {
			while(true){
				//is.wait(); //this throws an exception!!
				while(0<omConn.getInputStream().available()){
					is = new ObjectInputStream(omConn.getInputStream());
					String fix=(String)is.readObject();
					System.out.println(Thread.currentThread().getName()+" received fix message: "+fix);
					String[] fixTags=fix.split(";");
					int OrderId=-1;
					char MsgType;
					int OrdStatus;
					methods whatToDo=methods.dontKnow;
					//String[][] fixTagsValues=new String[fixTags.length][2];
					for(int i=0;i<fixTags.length;i++){
						String[] tag_value=fixTags[i].split("=");
						switch(tag_value[0]){
							case"11":OrderId=Integer.parseInt(tag_value[1]);break;
							case"35":MsgType=tag_value[1].charAt(0);
								if(MsgType=='A')whatToDo=methods.newOrderSingleAcknowledgement;
								if(MsgType=='F')whatToDo=methods.pendingCancelRequest;
								break;
							case"39":
								OrdStatus=tag_value[1].charAt(0);
								if(OrdStatus=='2')whatToDo=methods.fullyFilled;
								else if(OrdStatus=='1')whatToDo=methods.partialFill;
								else if(OrdStatus=='4')whatToDo=methods.cancelled;
								break;
						}
					}
					switch(whatToDo){

						case newOrderSingleAcknowledgement:newOrderSingleAcknowledgement(OrderId); break;
						case partialFill:partialFill(OrderId); break;
						case fullyFilled:fullyFilled(OrderId); break;
						case pendingCancelRequest:pendingCancelRequest(OrderId); break;
						case cancelled:cancelled(OrderId); break;
						default: System.out.println("methodname"+whatToDo.name());
					}

					//double elapsedtime=((double)OUT_QUEUE.get(OrderId).time.getNano()-LocalDateTime.now().getNano())/1000000000.0;
					//if(elapsedtime>=180) sendCancel(OrderId);
				}
			}
		} catch (IOException|ClassNotFoundException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void newOrderSingleAcknowledgement(int OrderId){
		System.out.println(Thread.currentThread().getName()+" called newOrderSingleAcknowledgement");
		//do nothing, as not recording so much state in the NOS class at present
	}
	@Override
	public void partialFill(int orderid){
		show(""+orderid);
	}
	
	@Override
	public void fullyFilled(int orderid){
		show(""+orderid);
		OUT_QUEUE.remove(orderid);
	}
	
	@Override
	public void pendingCancelRequest(int orderid){
		show(""+orderid);
	}
	
	@Override
	public void cancelled(int orderid){
		show(""+orderid);
		OUT_QUEUE.remove(orderid);
	}
/*listen for connections
once order manager has connected, then send and cancel orders randomly
listen for messages from order manager and print them to stdout.*/
}