import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;

import javax.net.ServerSocketFactory;

import Enum.traderApi;
import OrderManager.Order;

//10.20.40.67 - My IP
//10.20.40.61 - Your IP

// 3000 - My port

public class Trader extends Thread {
	private HashMap<Integer,Order> orders=new HashMap<Integer,Order>();
	private static Socket omConn;
	private int port;
	ObjectInputStream  is;
	ObjectOutputStream os;
	
	private static SocketChannel guiClient;
	
	private ArrayList<String> pendingMessages = new ArrayList<String>();
	
	private int tempId;
	private Order tempOrder;
	private boolean isCrossing = false;
	
	/**
	 * CONSTRUCTOR
	 * @param name Name of the thread when created.
	 * @param port The port number for the order manager to connect to.
	 */
	Trader(String name,int port){
		this.setName(name);
		this.port=port;
	}
	
	/**
	 * The main method that is called after the start of the thread.
	 * Runs the main logic for the class
	 */
	public void run(){
		//OM will connect to us
		try {
			omConn=ServerSocketFactory.getDefault().createServerSocket(port).accept();
			
			waitForGuiConnection();

			InputStream s=omConn.getInputStream();
			while(true){
				if (guiClient.isConnected()) {
					for (String message : pendingMessages) {
						writeToGUI(message);
					}
					readFromGUI();
				}
				
				if(0<s.available()){
					is=new ObjectInputStream(s);
					traderApi method=(traderApi)is.readObject();
					System.out.println(Thread.currentThread().getName()+" calling: "+method);
					switch(method){
						case newOrder:newOrder(is.readInt(),(Order)is.readObject());break;
						case price:price(is.readInt(),(Order)is.readObject());break;
						case cross: 
							if (isCrossing) {
								updateOrderCross(tempId, tempOrder, is.readInt(), (Order)is.readObject());
								isCrossing = false;
							} else {
								tempId = is.readInt();
								tempOrder = (Order)is.readObject();
								isCrossing = true;
							}
							break;
						case fill: 
							updateOrderFill(is.readInt(), (Order)is.readObject(), (Order)is.readObject()); break;
						case slice: 
							updateOrder(is.readInt(), (Order)is.readObject());break;
					}
				} else{
					Thread.sleep(1000);
				}
			}
		} catch (IOException | ClassNotFoundException | InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates a server socket and waits for the client GUI to connect.
	 * @throws IOException
	 */
	private void waitForGuiConnection() throws IOException {
		InetSocketAddress ourAddress=new InetSocketAddress(3000);
		ServerSocketChannel ss=ServerSocketChannel.open();
		ss.configureBlocking(false);
		ss.bind(ourAddress);
		while (guiClient == null) {
			guiClient = ss.accept();
		}
		ss.close();
		
		guiClient.configureBlocking(false);
		writeToGUI("Connected");
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	private void readFromGUI() throws Exception {
		ByteBuffer inputBuffer = ByteBuffer.allocate(1024);
		int amount = guiClient.read(inputBuffer);
		if (amount == 0) {
			return;
		}
		inputBuffer.flip();
		String message = "";
		byte currentByte = 0x0;
		
		for (int i = 0; i < amount; ++i) {
			currentByte = inputBuffer.get();
			message += (char)currentByte;
		}
		inputBuffer.clear();
		if (currentByte == '#') {
			translateGUIMessage(message);
		}
	}
	
	private void writeToGUI(String message) throws IOException {
		ByteBuffer outputBuffer = ByteBuffer.allocate(1024);
		outputBuffer.put(message.getBytes());
		outputBuffer.flip();
		
		guiClient.write(outputBuffer);
		outputBuffer.clear();
	}
	
	/**
	 * Receives a new order from the order manager and stores it in its orders list.
	 * Will send information about the order to the GUI. Order will be of status
	 * 'A' - pending new.
	 * @param id Id of the order being received.
	 * @param order The order that is being received.
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private void newOrder(int id, Order order) throws IOException, InterruptedException {
		if (orders.containsKey(id)) {
			orders.remove(id);
		}
		orders.put(id, order);
		if (guiClient.isConnected()) {
			writeToGUI(orderToMessage(id, order));
		} else {
			pendingMessages.add(orderToMessage(id, order));
		}
	}

	/**
	 * Updates the current status of an order to '0' - new. Will send a message
	 * to order manager to confirm the order has been accepted. Send an updated
	 * version of the order to the GUI.
	 * @param id Id of the order to be changed to accepted.
	 * @throws IOException
	 */
	private void acceptOrder(int id) throws IOException {
		Order order = orders.get(id);
		order.ordStatus = '0';
		os=new ObjectOutputStream(omConn.getOutputStream());
		os.writeObject("acceptOrder");
		os.writeInt(id);
		os.flush();

		if (guiClient.isConnected()) {
			writeToGUI(orderToMessage(id, order));
		} else {
			pendingMessages.add(orderToMessage(id, order));
		}
	}

	/**
	 * Gets the order information from the order manager and uses it to updates its own 
	 * internal storage. Sends the update to the GUI.
	 * @param id Id of the order to be updated.
	 * @param order The new state of the order.
	 * @throws IOException 
	 */
	private void updateOrderCross(int id, Order order, int secondId, Order secondOrder) throws IOException {
		if (orders.containsKey(id) && orders.containsKey(secondId)) {
			orders.remove(id);
			orders.put(id, order);
			orders.remove(secondId);
			orders.put(secondId, secondOrder);

			if (guiClient.isConnected()) {
				writeToGUI(orderToMessage(id, order) + orderToMessage(secondId, secondOrder));
			} else {
				pendingMessages.add(orderToMessage(id, order) + orderToMessage(secondId, secondOrder));
			}
		}
	}

	/**
	 * Gets the order information from the order manager and uses it to updates its own 
	 * internal storage. Sends the update to the GUI.
	 * @param id Id of the order to be updated.
	 * @param order The new state of the order.
	 * @throws IOException 
	 */
	private void updateOrder(int id, Order order) throws IOException {
		if (orders.containsKey(id)) {
			orders.remove(id);
			orders.put(id, order);

			if (guiClient.isConnected()) {
				writeToGUI(orderToMessage(id, order));
			} else {
				pendingMessages.add(orderToMessage(id, order));
			}
		}
	}

	/**
	 * Gets the order information from the order manager and uses it to updates its own 
	 * internal storage. Sends the update to the GUI.
	 * @param id Id of the order to be updated.
	 * @param order The new state of the order.
	 * @throws IOException 
	 */
	private void updateOrderFill(int id, Order order, Order slice) throws IOException {
		order.slices.remove(order.slices.size() - 1);
		order.slices.add(slice);
		if (orders.containsKey(id)) {
			orders.remove(id);
			orders.put(id, order);

			if (guiClient.isConnected()) {
				writeToGUI(orderToMessage(id, order));
			} else {
				pendingMessages.add(orderToMessage(id, order));
			}
		}
	}
	
	/**
	 * Sends a message to the order manager to perform an internal cross with
	 * the two orders of the ids given.
	 * @param buyId The id of the buy order.
	 * @param sellId The id of the sell order.
	 * @throws IOException
	 */
	private void crossOrder(int buyId, int sellId) throws IOException {
		os=new ObjectOutputStream(omConn.getOutputStream());
		os.writeObject("crossOrder");
		os.writeInt(buyId);
		os.writeInt(sellId);
		os.flush();
	}
	
	/**
	 * Sends a message to the order manager to send the order of the id given to the
	 * order router to get an external trade.
	 * @param id Id of the order to be traded.
	 * @throws IOException
	 */
	private void sendToRouter(int id) throws IOException {
		os=new ObjectOutputStream(omConn.getOutputStream());
		os.writeObject("routeOrder");
		os.writeInt(id);
		os.writeObject(orders.get(id));
		os.flush();
	}

	/**
	 * Sends a message to the order manager to send a slice of the order of the id given to the
	 * order router to get an external trade.
	 * @param id Id of the order to be traded.
	 * @param sliceId Id of the slice within the order to be traded.
	 * @throws IOException
	 */
	private void sendToRouter(int id, int sliceId) throws IOException {
		os=new ObjectOutputStream(omConn.getOutputStream());
		os.writeObject("routeOrderSlice");
		os.writeInt(id);
		os.writeInt(sliceId);
		os.writeObject(orders.get(id));
		os.flush();
	}
	
	/**
	 * Sends a message to the order manager to create a new slice from an order of a 
	 * specified size.
	 * @param id The id of the order to be sliced.
	 * @param sliceSize The size of the new slice.
	 * @throws IOException
	 */
	private void sliceOrder(int id, int sliceSize) throws IOException {
		os=new ObjectOutputStream(omConn.getOutputStream());
		os.writeObject("sliceOrder");
		os.writeInt(id);
		os.writeInt(sliceSize);
		os.flush();
	}
	
	private void price(int id,Order o) throws InterruptedException, IOException {
		//TODO should update the trade screen
		Thread.sleep(2134);
		sliceOrder(id,orders.get(id).sizeRemaining()/2);
	}
	
	private String orderToMessage(int id, Order order) {
		return "," + id + "," + order.initialMarketPrice + "," + order.sizeRemaining() + "," + order.sizeFilled() + "," + 
				order.sizeAtMarket() + "," + order.instrument.toString() + "," + order.side + "," + order.getOrdStatus() + "," + order.getOrdType() + ",#";
	}
	
	private void translateGUIMessage(String message) throws Exception {
		String[] segments = message.split(",");
		
		if (segments[0].equals("Slice")) {
			sliceOrder(Integer.parseInt(segments[1]), Integer.parseInt(segments[2]));
		} else if (segments[0].equals("Send")) {
			sendToRouter(Integer.parseInt(segments[1]));
		} else if (segments[0].equals("Cross")) {
			crossOrder(Integer.parseInt(segments[1]), Integer.parseInt(segments[2]));
		} else if (segments[0].equals("Cancel")) {
			// TODO cant cancel orders yet
		} else if (segments[0].equals("Accept")) {
			acceptOrder(Integer.parseInt(segments[1]));
		} else {
			throw new Exception("Invalid message tag: " + segments[0]);
		}
	}
}
