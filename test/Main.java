import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.util.Properties;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import LiveMarketData.LiveMarketData;
import LiveMarketData.LiveMarketDataThread;
import OrderManager.OrderManager;

public class Main{
	 
	public static void main(String[] args) throws IOException{
		System.out.println("TEST: this program tests ordermanager");
		Properties prop = new Properties();
		InputStream input = new FileInputStream("config.properties");
		prop.load(input);
		
		SampleClient.generateInstrumentsdata();
		//start sample clients
		(new MockClient("Client 1",Integer.parseInt(prop.getProperty("client1SocketAddress")))).start();
		(new MockClient("Client 2",Integer.parseInt(prop.getProperty("client2SocketAddress")))).start();
		
		//start sample routers
		(new SampleRouter("Router LSE",Integer.parseInt(prop.getProperty("routerLSESocketAddress")))).start();
		(new SampleRouter("Router BATE",Integer.parseInt(prop.getProperty("routerBATESocketAddress")))).start();
	
		(new Trader("Trader James",Integer.parseInt(prop.getProperty("traderSocketAddress")))).start();
		//start order manager
		InetSocketAddress[] clients={new InetSocketAddress(Integer.parseInt(prop.getProperty("client1SocketAddress"))),
		                     new InetSocketAddress(Integer.parseInt(prop.getProperty("client2SocketAddress")))};
		InetSocketAddress[] routers={new InetSocketAddress(Integer.parseInt(prop.getProperty("routerLSESocketAddress"))),
		                     new InetSocketAddress(Integer.parseInt(prop.getProperty("routerBATESocketAddress")))};
		InetSocketAddress trader=new InetSocketAddress(Integer.parseInt(prop.getProperty("traderSocketAddress")));
		InetSocketAddress marketData=new InetSocketAddress(Integer.parseInt(prop.getProperty("liveMarketDataSocketAddress")));
		new LiveMarketDataThread().start();
		(new MockOM("Order Manager",routers,clients,trader,marketData)).start();
	}
	//
	//public static void newClients(int port) {
		//create new client name
	//	new MockClient("Client New",port).start();
	//}
}


class MockClient extends Thread{
	int port;
	MockClient(String name, int port){
		this.port=port;
		this.setName(name);
	}
	public void run(){
		SampleClient client=new SampleClient(port);
		client.sendOrder();
		client.sendOrder();
		client.sendOrder();
		client.sendOrder();
		client.sendOrder();
		client.sendOrder();
		client.sendOrder();
		client.sendOrder();
		client.sendOrder();
		client.sendOrder();

		client.messageHandler();
	}
}

class MockOM extends Thread{
	InetSocketAddress[] clients;
	InetSocketAddress[] routers;
	InetSocketAddress trader;
	InetSocketAddress marketData;
	MockOM(String name,InetSocketAddress[] routers,InetSocketAddress[] clients,InetSocketAddress trader,InetSocketAddress marketData){
		this.clients=clients;
		this.routers=routers;
		this.trader=trader;
		this.marketData=marketData;
		this.setName(name);
	}
	@Override
	public void run(){
		try{
			//In order to debug constructors you can do F5 F7 F5
			new OrderManager(routers,clients,trader);
		}catch(IOException | ClassNotFoundException | InterruptedException ex){
			Logger.getLogger(MockOM.class.getName()).log(Level.SEVERE,null,ex);
		}
	}
}