import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.net.ServerSocketFactory;

import OrderRouter.Router;
import Ref.Instrument;
import Ref.Ric;

public class SampleRouter extends Thread implements Router{
	private static final Instrument[] INSTRUMENTS={new Instrument(new Ric("VOD.L")), new Instrument(new Ric("BP.L")), new Instrument(new Ric("BT.L"))};
	private Socket omConn;
	private int port;
	public SampleRouter(String name,int port){
		this.setName(name);
		this.port=port;
	}
	ObjectInputStream is;
	ObjectOutputStream os;
	public void run(){
		//OM will connect to us
		try {
			omConn=ServerSocketFactory.getDefault().createServerSocket(port).accept();
			while(true){
				if(0<omConn.getInputStream().available()){
					is=new ObjectInputStream(omConn.getInputStream());
					Router.routerApi methodName=(Router.routerApi)is.readObject();
					System.out.println("Order Router recieved method call for:"+methodName);
					switch(methodName){
						case routeOrder:routeOrder(is.readInt(),is.readInt(),is.readInt(),(Instrument)is.readObject());break;
						case priceAtSize:priceAtSize(is.readInt(),is.readInt(),(Instrument)is.readObject(),is.readInt());break;
					default: System.out.println("Unknown method.. "); break;
					}
				}else{
					Thread.sleep(100);
				}
			}
		} catch (IOException | ClassNotFoundException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void routeOrder(int id,int sliceId,int size,Instrument i) throws IOException, InterruptedException{ //MockI.show(""+order);
		//TODO have this similar to the market price of the instrument
		Thread.sleep(42);
		os=new ObjectOutputStream(omConn.getOutputStream());
		os.writeObject("newFill");
		os.writeInt(id);
		os.writeInt(sliceId);
		os.writeInt(size);
		os.writeDouble(checkPrice(i.toString()));
		os.flush();
	}

	@Override
	public void sendCancel(int id,int sliceId,int size,Instrument i) throws IOException{ //MockI.show(""+order);
		//os=new ObjectOutputStream(omConn.getOutputStream());
		//if(possible to cancel) 
			//os.writeObject("cancelled");
			//os.flush();
		//else send rejectedcancel
	}
	@Override
	public void priceAtSize(int id, int sliceId,Instrument i, int size) throws IOException{
		os=new ObjectOutputStream(omConn.getOutputStream());
		os.writeObject("bestPrice");
		os.writeInt(id);
		os.writeInt(sliceId);
		os.writeDouble(checkPrice(i.toString()));
		os.flush();
	}
	/**
	 * This method takes a RIC as an input the scours the instrumentsdata.txt file to see if a matching RIC is found. If so, it returns the price, raised or lowered by up to 3%.
	 * If a match is not found, the price returned is 0.
	 * @param ric
	 * @return
	 */
	public double checkPrice(String ric) {
		Scanner scanner;
		ArrayList<String[]> prices = new ArrayList<>();
		try {
			scanner = new Scanner(new File("instrumentsdata.txt"));
			while (scanner.hasNextLine()) {
				prices.add(scanner.nextLine().split(" "));
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (String[] result : prices) {
			if (result[0].equals(ric)) {
				double price = Double.parseDouble(result[1]);
				Random rng = new Random();
				switch(rng.nextInt(2)) {
				case 0: return price-(rng.nextDouble()*(price*0.03));
				case 1: return price+(rng.nextDouble()*(price*0.03));
				}
			}
		}
		return 0;
	}
}