from tkinter import *

BUTTONWIDTH = 16
LISTBOXHEIGHT = 18


class SlicePopup(object):
    value = 0

    def __init__(self):

        self.top = Toplevel()

        Label(self.top, text="How many units to slice").pack()

        self.e = Entry(self.top)
        self.e.pack(padx=2)

        b = Button(self.top, text="Slice", command=self.ok, width=BUTTONWIDTH)
        b.pack(pady=5)

    def ok(self):
        self.value = self.e.get()
        self.top.destroy()
        

class CrossPopup(object):

    selectedOrder = None

    def __init__(self, orders):

        self.orders = orders

        self.top = Toplevel()

        self.cross_listbox = Listbox(self.top, height=LISTBOXHEIGHT, width=25,  background="White", fg="Black",selectbackground="Black")
        self.cross_listbox.pack(side=LEFT)

        for order in orders:
            if int(order[2]) > 0: 
                # Convert buy or sell from binary.
                if order[6] == '0':
                    orderType = "Buy"
                else:
                    orderType = "Sell"
                # Make order readable.
                orderinformation = "{0} {1} {2} @ {3}".format(orderType, order[2], order[5], order[1])
                self.cross_listbox.insert(END, orderinformation)

        b = Button(self.top, text="Cross", command=self.ok, width=BUTTONWIDTH)
        b.pack()

    def ok(self):
        try:
            self.selectedOrder = self.orders[self.cross_listbox.curselection()[0]]
        except:
            self.selectedOrder = None
        self.top.destroy()
        
