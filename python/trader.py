from tkinter import *
from popup import *
from client import *

BUTTONWIDTH = 16
LISTBOXHEIGHT = 36

class TraderGui(object):

    def __init__(self):

        self.orders = []

        # Create order lists
        self.pendingneworders = []
        self.neworders = []

        # Set up window
        self.window = Tk()
        self.window.title('Trader')
        self.window.geometry("600x600") 
        self.window.resizable(0, 0)

        # Set up frames
        self.pendingnew_frame = Frame(self.window)
        self.pendingnew_frame.pack(side=LEFT)

        self.new_frame = Frame(self.window)
        self.new_frame.pack()

        # Set up pending new order list
        self.pendingnew_label = Label(self.pendingnew_frame, text="Pending New Orders")
        self.pendingnew_label.pack()
        self.pendingnew_listbox = Listbox(self.pendingnew_frame, height=LISTBOXHEIGHT, width=25,  background="White", fg="Black",selectbackground="Black")
        self.pendingnew_listbox.pack(side=LEFT)

        # Add pending new buttons
        self.accept_button = Button(self.pendingnew_frame, text="Accept Order", width=BUTTONWIDTH, command=self.acceptOrderButton)
        self.accept_button.pack()

        # Set up new order list
        self.new_label = Label(self.new_frame, text="New Orders")
        self.new_label.pack()
        self.new_listbox = Listbox(self.new_frame, height=LISTBOXHEIGHT, width=25,  background="White", fg="Black",selectbackground="Black")
        self.new_listbox.pack(side=LEFT)

        # Add new order buttons
        # self.slice_button = Button(self.new_frame, text="Slice", width=BUTTONWIDTH, command=self.sliceButton)
        # self.slice_button.pack()

        self.send_button = Button(self.new_frame, text="Send to Ex", width=BUTTONWIDTH, command=self.sendToExchangeButton)
        self.send_button.pack()

        # self.cancel_button = Button(self.new_frame, text="Cancel", width=BUTTONWIDTH, command=)
        # self.cancel_button.pack()

        self.cross_button = Button(self.new_frame, text="Cross", width=BUTTONWIDTH, command=self.crossButton)
        self.cross_button.pack()

        # Connect to server.
        self.client = Client()
        self.orders = self.client.orders


    def populateListBoxWithOrders(self, listbox, orders):
        listbox.delete(0,END)
        # ID, Price, TotalSize, SizeFilled, SizeMarket, Instrument(RIC), Bid/Ask(0/1), OrderStatus, location(0 local, 1 market)
        # OrdStatus is Fix 39, 'A' = Pending new, '1' = Partial fill, '2' = Filled, '0' = new
        for order in orders:
            if int(order[2]) > 0: 
                # Convert buy or sell from binary.
                if order[6] == '0':
                    orderType = "Buy"
                else:
                    orderType = "Sell"
                # Make order readable.
                orderinformation = "{0} {1} {2} @ {3:.2f}".format(orderType, int(order[2]), order[5], float(order[1]))
                listbox.insert(END, orderinformation)

    def updateOrders(self, orders):
        self.pendingneworders = []
        self.neworders = []
        # Sort orders.
        for order in orders:
            if order[7] == 'A':
                self.pendingneworders.append(order)
            elif order[7] == '0' or order[7] == '1' or order[7] == '2':
                self.neworders.append(order)

        self.displayOrders()

    def displayOrders(self):
        self.populateListBoxWithOrders(self.pendingnew_listbox, self.pendingneworders)
        self.populateListBoxWithOrders(self.new_listbox, self.neworders)

    # Button handlers ----------------------------------------------------------------------------------------------------------------
 
    def acceptOrderButton(self):
        index = self.pendingnew_listbox.curselection()[0]
        self.client.acceptOrder(self.pendingneworders[index])
        self.orders = self.client.orders
        self.updateOrders(self.orders)

    def sliceButton(self):
        index = self.new_listbox.curselection()[0]
        popup = SlicePopup()
        self.window.wait_window(popup.top)
        
        # Validate.
        try:
            amountToSlice = int(popup.value)
            if amountToSlice < self.neworders[index][2]:
                self.client.sliceRequest(self.neworders[index], amountToSlice)
            else:
                print("Cannot slice off a chunk greater than or equal to the initial size.")
         
        except:
            print("Slice value has to be an integer")

    def sendToExchangeButton(self):
        index = self.new_listbox.curselection()[0]
        self.client.sendToExchange(self.neworders[index])
        self.orders = self.client.orders
        self.updateOrders(self.orders)

    def cancelButton(self):
        index = self.new_listbox.curselection()[0]
        self.client.cancel(self.neworders[index])

    def crossButton(self):
        index = self.new_listbox.curselection()[0]
        order = self.neworders[index]

        possibleCrosses = []

        for otherorder in self.neworders:
            if int(otherorder[6]) != int(order[6]): # Looks for sell orders if buy and buy orders if sell.

                if otherorder[5].split(".")[0] == order[5].split(".")[0]: # Match instruments.
                    if otherorder[6] == '0' and float(otherorder[1]) >= float(order[1]): # If buy then price should be higher.
                        possibleCrosses.append(otherorder)
                    if otherorder[6] == '1' and float(otherorder[1]) <= float(order[1]): # If sell then price should be lower.
                        possibleCrosses.append(otherorder)

        crosspopup = CrossPopup(possibleCrosses)
        self.window.wait_window(crosspopup.top)

        if crosspopup.selectedOrder != None:
            if crosspopup.selectedOrder[6] == '0':
                orderType = "Buy"
            else:
                orderType = "Sell"

            if order[6] == '0': # Order is buy
                self.client.crossRequest(order[0],crosspopup.selectedOrder[0])
            else: 
                self.client.crossRequest(crosspopup.selectedOrder[0], order[0])  
        else:
            print("Cross is not valid")

        self.orders = self.client.orders
        self.updateOrders(self.orders)