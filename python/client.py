import socket, time

SERVER_IPADDRESS = "localhost"
SERVER_PORT = 3000

class Client(object):

    connected = False
    orders = []

    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        host = SERVER_IPADDRESS
        port = SERVER_PORT

        try:
            self.s.connect((host, port))
            self.connected = True

            data = self.s.recv(1024)
            if "Connected" in data.decode():
                print("Connected")
                time.sleep(0.5)
                self.orders = self.receiveOrders()
        
        except:
            print("Connection failed")

    def sendData(self, data):
        try:
            self.s.send(data.encode("UTF-8"))
            self.updateOrders(self.receiveOrders())
        except:
            print("Sending failed.")

    def acceptOrder(self, order):
        data = "Accept,{0},#".format(order[0])
        self.sendData(data)

    def sliceRequest(self, order, amountToSlice):
        data = "Slice,{0},{1},#".format(order[0],amountToSlice)
        self.sendData(data)

    def sendToExchange(self, order):
        data = "Send,{0},#".format(order[0])
        self.sendData(data)

    def cancel(self, order):
        data = "Cancel,{0},{1},#".format(order[0], order[8])
        self.sendData(data)

    def crossRequest(self, buyId, sellId):
        data = "Cross,{0},{1},#".format(buyId, sellId)
        self.sendData(data)

    def receiveOrders(self):
        orders = []
        receiving = True

        data = self.s.recv(1024)
        arrdata = data.decode().split(",")
        arrdata.pop(0) # Remove first element ''
        print(arrdata)
        order = []
        for element in arrdata:
            if "#" in element:
                orders.append(order)
                order = []
            else:
                order.append(element)

        return orders

    def updateOrders(self, updatedorders):
        for j in range(len(self.orders)):
            for i in range(len(updatedorders)):
                if int(updatedorders[i][0]) == int(self.orders[j][0]):
                    self.orders[j] = updatedorders[i]


        

